package com.example.indicadores;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends AppCompatActivity {

    private TextView tvChillan;
    private TextView tvMadrid;
    private TextView tvParis;
    private TextView tvMoscu;
    private TextView tvPresion;
    private TextView tvPresion2;
    private TextView tvPresion3;
    private TextView tvPresion4;
    private TextView tvHumedad;
    private TextView tvHumedad2;
    private TextView tvHumedad3;
    private TextView tvHumedad4;
    private TextView tvTemperatura;
    private TextView tvTemperatura2;
    private TextView tvTemperatura3;
    private TextView tvTemperatura4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.tvChillan = (TextView) findViewById(R.id.tvChillan);
        this.tvMadrid = (TextView) findViewById(R.id.tvMadrid);
        this.tvParis = (TextView) findViewById(R.id.tvParis);
        this.tvMoscu = (TextView) findViewById(R.id.tvMoscu);
        this.tvPresion = (TextView) findViewById(R.id.tvPresion);
        this.tvPresion2 = (TextView) findViewById(R.id.tvPresion2);
        this.tvPresion3 = (TextView) findViewById(R.id.tvPresion3);
        this.tvPresion4 = (TextView) findViewById(R.id.tvPresion4);
        this.tvHumedad = (TextView) findViewById(R.id.tvHumedad);
        this.tvHumedad2 = (TextView) findViewById(R.id.tvHumedad2);
        this.tvHumedad3 = (TextView) findViewById(R.id.tvHumedad3);
        this.tvHumedad4 = (TextView) findViewById(R.id.tvHumedad4);
        this.tvTemperatura = (TextView) findViewById(R.id.tvTemperatura);
        this.tvTemperatura2 = (TextView) findViewById(R.id.tvTemperatura2);
        this.tvTemperatura3 = (TextView) findViewById(R.id.tvTemperatura3);
        this.tvTemperatura4 = (TextView) findViewById(R.id.tvTemperatura4);

        // CHILLÁN
        String url = "http://api.openweathermap.org/data/2.5/weather?lat=-36.6&lon=-72.1167&appid=2ec8c33c02bae39e3292f3a80d6f490b&units=metric";
        StringRequest solicitud = new StringRequest(

                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Tenemos respuesta desde el servidor
                        try {


                            JSONObject jsonObject = new JSONObject(response);

                            JSONObject weatherData = new JSONObject(jsonObject.getString("main"));



                            double tempInt = Double.parseDouble(weatherData.getString("temp"));
                            double centi = tempInt;
                            centi = Math.round(centi);
                            int i = (int)centi;
                            double humedad = Double.parseDouble(weatherData.getString("humidity"));
                            double presion = Double.parseDouble(weatherData.getString("pressure"));






                            tvTemperatura.setText("Temperatura " + i + "°");
                            tvHumedad.setText("Humedad "+humedad + "%");
                            tvPresion.setText("Presión " + presion + "hPa");


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }





                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }
        );
        // Paris
        String url2 = "http://api.openweathermap.org/data/2.5/weather?lat=48.8534000&lon=2.3486000&appid=2ec8c33c02bae39e3292f3a80d6f490b&units=metric";
        StringRequest solicitud2 = new StringRequest(

                Request.Method.GET,
                url2,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Tenemos respuesta desde el servidor
                        try {


                            JSONObject jsonObject = new JSONObject(response);

                            JSONObject weatherData = new JSONObject(jsonObject.getString("main"));



                            double tempInt = Double.parseDouble(weatherData.getString("temp"));
                            double centi = tempInt;
                            centi = Math.round(centi);
                            int i = (int)centi;
                            double humedad = Double.parseDouble(weatherData.getString("humidity"));
                            double presion = Double.parseDouble(weatherData.getString("pressure"));






                            tvTemperatura2.setText("Temperatura " + i + "°");
                            tvHumedad2.setText("Humedad "+humedad + "%");
                            tvPresion2.setText("Presión " + presion + "hPa");


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }





                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }


);

        // Moscú
        String url3 = "http://api.openweathermap.org/data/2.5/weather?lat=55.7522202&lon=37.6155586&appid=2ec8c33c02bae39e3292f3a80d6f490b&units=metric";
        StringRequest solicitud3 = new StringRequest(

                Request.Method.GET,
                url3,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Tenemos respuesta desde el servidor
                        try {


                            JSONObject jsonObject = new JSONObject(response);

                            JSONObject weatherData = new JSONObject(jsonObject.getString("main"));



                            double tempInt = Double.parseDouble(weatherData.getString("temp"));
                            double centi = tempInt;
                            centi = Math.round(centi);
                            int i = (int)centi;
                            double humedad = Double.parseDouble(weatherData.getString("humidity"));
                            double presion = Double.parseDouble(weatherData.getString("pressure"));






                            tvTemperatura3.setText("Temperatura " + i + "°");
                            tvHumedad3.setText("Humedad "+humedad + "%");
                            tvPresion3.setText("Presión " + presion + "hPa");


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }





                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }


        );


// Moscú
        String url4 = "http://api.openweathermap.org/data/2.5/weather?lat=40.4167&lon=-3.70325&appid=2ec8c33c02bae39e3292f3a80d6f490b&units=metric";
        StringRequest solicitud4 = new StringRequest(

                Request.Method.GET,
                url4,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Tenemos respuesta desde el servidor
                        try {


                            JSONObject jsonObject = new JSONObject(response);

                            JSONObject weatherData = new JSONObject(jsonObject.getString("main"));



                            double tempInt = Double.parseDouble(weatherData.getString("temp"));
                            double centi = tempInt;
                            centi = Math.round(centi);
                            int i = (int)centi;
                            double humedad = Double.parseDouble(weatherData.getString("humidity"));
                            double presion = Double.parseDouble(weatherData.getString("pressure"));






                            tvTemperatura4.setText("Temperatura " + i + "°");
                            tvHumedad4.setText("Humedad "+humedad + "%");
                            tvPresion4.setText("Presión " + presion + "hPa");


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }





                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // Algo fallo
                    }
                }


        );





        RequestQueue listaEspera = Volley.newRequestQueue(getApplicationContext());
        listaEspera.add(solicitud);
        listaEspera.add(solicitud2);
        listaEspera.add(solicitud3);
        listaEspera.add(solicitud4);





    }

}
